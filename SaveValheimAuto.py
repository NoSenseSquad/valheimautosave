import ftplib
import datetime
import time
import os
import sys
import msvcrt
import string

###READ ME#####
"""
Lancer la comande python SaveValheimAuto.py [-auto]

"""

######### paramètre ###############
## Nom de du serveur FTP hôte
host = ""
## Nom du compte utilisateur
user = ""
## Mot de passe
password = ""
## nom de la map
mapName = "Dedicated"

## (optionel) a modifier si vous voulez changer le répertoire de destination des sauvegardes
specificPath =r"c:\Valheim Save"
## (optionel) le temps en heure entre chaque copie
## ATTENTION ne pas trop réduire cela pourrait surcharger le serveur ftp et rapidement prendre de la place sur votre pc !
hourSave = 12




######## Code ################
#var
isAuto = False


#See if auto
if len(sys.argv)>1 :
    isAuto = "-auto" in sys.argv[1]




### Création d'un dossier save si nécessaire avec la date et heure de l'execution de la copie
def make_directory_if_not_exists(path):
    if not os.path.isdir(path):
        os.makedirs(path)


### Copie des fichier via FTP
def transfer_FTP_Files(path,connect):
    print("Transfert du fichier .fwl")
    try:
        connect.retrbinary("RETR " + mapName+".fwl",open(path+"\\"+mapName+".fwl", 'wb').write)
        print("Fichier .fwl copié avec succès !")
    except:
        print("Error transferring .fwl file")
    print("\nTransfert du fichier .db")
    try:
        connect.retrbinary("RETR " + mapName+".db",open( path+"\\"+mapName+".db", 'wb').write)
        print("Fichier .db copié avec succès !")
    except:
        print("Error transferring .db file")
        
   
def MakeCopy():
    connect = ftplib.FTP(host,user,password)
    date = datetime.datetime.now().strftime("%d%m%Y-%Hh%M")
    path=os.path.join(specificPath,"Save_"+date)
    print(''.join(['#' for i in range(20)]))
    print("la save sera dans le dossier : " + path +"\n")
    connect.cwd("/.config/unity3d/IronGate/Valheim/worlds")
    make_directory_if_not_exists(path)
    transfer_FTP_Files(path,connect)
    connect.quit()
    

if isAuto :
    secondSave = hourSave * 3600
    sec = secondSave
    while(True) :
        try:
            time.sleep(1)       
            if sec == secondSave :
                sec = 0
                MakeCopy()
            elif(sec%900==0):
                print("Appuyer sur crtl+c pour quitter, la prochaine copie sera dans "+ str((secondSave - sec)//3600)+"h" +str(((secondSave - sec)%3600)//60)+"m" )
                sec +=1
            else :
                sec +=1
        except KeyboardInterrupt:
            print("sauvegarde auto arrêté !")
            break
else :
    MakeCopy()





