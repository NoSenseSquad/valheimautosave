### ValheimAutoSave

## Introduction 

Petit srcript qui une fois paramétré permttra de faire une copie du monde valheim depuis le serveur hébergé vers votre pc en local.
Ce script utilise le protocal ftp.
C'est un petit script fait rapidement donc si ca crash rin d'anormal n'hésitez pas a me prévenir ;)

## Installation

* Installer python 3.9 via le microsft store
* Télecharger la dernière release en .zip
* Décompresser le fichier qu'importe son emplacement
* Ouvrir le fichier le fichier avec note pad (je conseille notepad ++)
* modifier les champs paramètres, des explications sont fourni dans le fichier pour vous guider

## Usage

* faire shift + clic droit sur le fichier selectionner "ouvrir la fenêtre powershell ici"
* lancer la commande "python SaveValheimAuto.py" 
* ajouter -auto a la fin de la commande si vous vous voulez des save récurente ATTENTION aux paramètres dans le fichier

## Remarque

* Atention le mot de passe sera donc stocké en clair dans le fichier rien de trop grave en soit mais c'est pas dingue niveau sécurité des malware non plus.
